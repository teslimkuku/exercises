/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit2.task3;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Teslim
 */
public class SceneTest {

    public SceneTest() {
    }

    @Test
    public void testSearchObject() {
        
        Scene scene = new Scene(5,2);
        scene.addObject("A", 0, 0);
        scene.addObject("B", 1, 1);
        scene.addObject("M", 2, 1);
        scene.addObject("P", 3, 1);
        scene.addObject("J", 4, 1);
        
        assertEquals(true, scene.searchObject("J",5,2));
    }
}
