/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sayhello;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Teslim
 */
public class ConverterTest {

    /**
     * Test of convertCelsiusToFahrenheit method, of class Converter.
     */
    @Test
    public void testConvertCelsiusToFahrenheit() {
        assertEquals(32.0, Converter.convertCelsiusToFahrenheit(0.0), 0.001);
        assertEquals(212.0, Converter.convertCelsiusToFahrenheit(100.0), 0.001);
        assertEquals(33.8, Converter.convertCelsiusToFahrenheit(1.0), 0.001);
    }   
}