/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Teslim
 */
public class SceneTest {

    /**
     * Test of getHeight method, of class Scene.
     */
    @Test
    public void testGetHeight() {
        //Scene scene = new Scene(10, 10);
        //assertEquals(10, scene.getWidth());
        //Scene scene2 = new Scene(10, 5);
        //assertEquals(10, scene2.getHeight ());

        //Scene scene = new Scene (5, 3);
        //scene.addTerrain("G", 2, 1);
        //scene.addTerrain("T", 4, 2);
        //assertEquals("G", scene.getTerrain(2, 1));
        //assertEquals("T", scene.getTerrain(4, 2));
        //Scene scene = new Scene(10,5);
        //scene.addObject("Book",5,4);
        //assertEquals("Book", scene.getObject(5, 4));
    }

    @Test
    public void testDistance() {

        Scene scene = new Scene(3,5);
        scene.addTerrain("A", 0, 1);
        scene.addTerrain("B", 0, 2);
        scene.addTerrain("C", 2, 1);
        scene.addTerrain("D", 2, 4);
        
        //assertEquals(false, scene.isEmpty(2, 1));
        
        assertEquals(2, scene.countItems(0, 2, 1));
        
        //scene.addObject("A", 0, 1);
        //scene.addObject("B", 0, 2);
        //scene.addObject("C", 2, 1);
        //scene.addObject("D", 2, 4);

        //assertEquals(1, Scene.distance(0,1,0,2)); //A -> B = 1
        //assertEquals(2, Scene.distance(0,1,2,1)); //A -> C = 2
        //assertEquals(2, scene.distance(2,1,0,1)); //C -> A = 2
        //assertEquals(5, Scene.distance(0,1,2,4)); //A -> D = 5
        //assertEquals(5, Scene.distance(2,4,0,1)); //D -> A = 5
        //assertEquals(0, 0);
    }
}