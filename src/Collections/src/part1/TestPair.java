/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package part1;

/**
 *
 * @author kukut
 */
public class TestPair {

    public static void main(String[] args) {
        Pair pair = new Pair("abc", new Integer(2));                  // <1>

        // retrieve the items: (String) and (Integer) are the required 'casts'
        String s = (String) pair.getA();                                // <2>
        Integer i = (Integer) pair.getB();                              // <3>

        System.out.println("Pair contains: " + s + " and " + i);
    }
}