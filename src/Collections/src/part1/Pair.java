/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package part1;

/**
 *
 * @author kukut
 */
public class Pair {

    // set up two private fields for the objects to store
    private Object a;                                               // <1>
    private Object b;

    // our constructor takes two objects to store
    public Pair(Object a, Object b) {                              // <2>
        this.a = a;
        this.b = b;
    }

    // accessor to the first object
    public Object getA() {                                         // <3>
        return a;
    }

    // accessor to the second object
    public Object getB() {
        return b;
    }
}