/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit2.task1;

/**
 *
 * @author Teslim
 */
public class TemperatureConversions {

    public double convertCelsiusToFahrenheit (double celsius) {
        return (celsius * 9 / 5) + 32;
    }
    
    public double convertFarenheitToCelcius(double fahrenheit) {

        double celcius = (fahrenheit - 32) * 5 / 9;
        return celcius;
    }
    
    public void temperatureConversionTable2(double start, double end, double stepSize) {

        System.out.println("Cel.\tFah");
        for (double i = start; i <= end; i += stepSize) {

            System.out.printf("%.0f\t%.0f\n", i, convertCelsiusToFahrenheit(i));
        }
    }

    public void temperatureConversionTable1(double start, double end, double stepSize) {

        System.out.println("Fah.\tCel.");
        for (double i = start; i <= end; i += stepSize) {

            System.out.printf("%.0f\t%.0f\n", i, convertFarenheitToCelcius(i));
        }
    }

    public static void main(String[] args) {
        TemperatureConversions tc = new TemperatureConversions();
        
        try {

            if (args.length == 2) {

                String w = args[0];
                String x = args[1];
                
                int type = Integer.parseInt(w);
                double temp = Double.parseDouble(x);
                
                if(type == 1){
                    
                    System.out.printf("Celsius: %.2f\n",tc.convertFarenheitToCelcius(temp));
                }else if(type == 2){
                    
                     System.out.printf("Fahrenheit: %.2f\n",tc.convertCelsiusToFahrenheit(temp));
                }else{
                    System.out.println("Unknown conversion type");
                    usage();
                    
                }
                
            } else if (args.length == 4) {

                String w = args[0];
                String x = args[1];
                String y = args[2];
                String z = args[3];

                int type = Integer.parseInt(w);
                double start = Double.parseDouble(x);
                double end = Double.parseDouble(y);
                double stepSize = Double.parseDouble(z);
                
                if(type == 1){
                    tc.temperatureConversionTable1(start, end, stepSize);
                }else if(type == 2){
                    tc.temperatureConversionTable2(start, end, stepSize);
                }else{
                    System.out.println("Unknown conversion type");
                    usage();
                }
            } else {
                System.out.println("Ivalid number of arguments");
                usage();
            }

        } catch (Exception e) {
            System.out.println("An error occured: " + e.getMessage());
        }
    }
    
    private static void usage(){
        System.out.println("***** Instructions *****");
        System.out.println("This is a temperature coversion application.");
        System.out.println("The program takes 2 or 4 command line arguments");
        System.out.println("The first argument(1 or 2) in either case is the type of conversion.");
        System.out.println("1 for fahrenheit to celcius and 2 for celcius to fahrenheit.");
        System.out.println("When you provide 2 arguments, the 2nd argument is the temperature to convert.");
        System.out.println("When you provide 4 arguments, the 2nd, 3rd and 4th arguments represents ");
        System.out.println("the start, stop and step respectively for the table of temperature conversion that will be displayed.");
    }
}
/**
 * Write a program to generate a table of temperature conversions, converting
 * from fahrenheit to celsius:
 *
 * Write a function to convert a single temperature from fahrenheit to celsius.
 * If F is the temperature in degrees fahrenheit, the temperature in degrees
 * celsius is (F-32)*5/9.
 *
 * Write a function which takes a start, end and step size, and prints a table
 * of temperature conversions. For example, if you call it with start 5, end 10
 * and step 2, it will print a line for 5, 7 and 9.
 *
 * Call your functions with different values, print the results, and make sure
 * they are working correctly.
 */
