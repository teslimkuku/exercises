/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit2.task3;

/**
 *
 * @author Teslim
 */
public class Scene {

    private int height;
    private int width;
    private String[][] terrain;
    private String[][] object;

    public Scene(int height, int width) {
        this.height = height;
        this.width = width;
        terrain = new String[height][width];
        object = new String[height][width];
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void addTerrain(String terrain, int row, int column) {
        this.terrain[row][column] = terrain;
    }

    public String getTerrain(int row, int column) {
        return this.terrain[row][column];
    }

    public void addObject(String object, int row, int column) {
        this.object[row][column] = object;
    }

    public String getObject(int row, int column) {
        return this.object[row][column];
    }

    public int distance(int r1, int c1, int r2, int c2) {

        int verticalDistance = r1 > r2 ? r1 - r2 : r2 - r1;
        int horizontalDistance = c1 > c2 ? c1 - c2 : c2 - c1;

        return verticalDistance + horizontalDistance;
    }

    /**
     * Check if a given cell is empty
     */
    public boolean isEmpty(int row, int column) {
        return terrain[row][column] == null;
    }

    /**
     * Count the cells which are not empty
     */
    public int countItems(int row, int column, int maxDistance) {
        int count = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (!isEmpty(i, j)) {
                    if (distance(i,j,row,column) <= maxDistance) {
                        count += 1;
                    }
                }
            }
        }
        return count;
    }
    
    public boolean searchObject(String obj, int row, int column){
        
        boolean objectFound = false;
        
        for(int i = 0; i < row; i++){
            
            for(int j = 0; j < column; j++){
                
                if(object[i][j] == obj){
                    objectFound = true;
                    break;
                }
            }
        }
        
        return objectFound;
    }
}