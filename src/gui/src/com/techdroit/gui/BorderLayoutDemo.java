/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit.gui;

/**
 *
 * @author kukut
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class BorderLayoutDemo extends JFrame {

    public BorderLayoutDemo () {
        super ("Example Swing program with BorderLayout");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout (new BorderLayout ());                        // <1>

        // we will place an editor pane in the middle
        JEditorPane pane = new JEditorPane ();
        JScrollPane jsp = new JScrollPane (pane); // wrap editor in a scrolling container
        add(jsp, BorderLayout.CENTER);                          // <2>

        // a title is placed on top
        JLabel label = new JLabel ("Enter some text in this window");
        add (label, BorderLayout.NORTH);                        // <3>

        // a checkbox on the right
        JCheckBox check = new JCheckBox ("Some option");
        add (check, BorderLayout.EAST);                         // <4>

        // button is placed on bottom
        JButton button = new JButton ("Describe everything");
        button.addActionListener (new DescribeAction (pane, check));  // <5>
        add (button, BorderLayout.SOUTH);                       // <6>

        setSize (400, 200);
    }

    public static void mainn(String[] args) {
        BorderLayoutDemo app = new BorderLayoutDemo();
        app.setVisible (true);
    }
}

class DescribeAction implements ActionListener {
    private final JEditorPane pane;
    private final JCheckBox check;

    public DescribeAction (JEditorPane pane, JCheckBox check) {
        this.pane = pane;                                           // <7>
        this.check = check;
    }

    public void actionPerformed (ActionEvent e) {
        System.out.println("Describing the state of the controls");

        // show text from pane
        System.out.println ("You have typed:");
        System.out.println (pane.getText());                        // <8>

        // see if check box is ticked
        if (check.isSelected()) {                                   // <9>
            System.out.println("You have ticked the option");
        } else {
            System.out.println("You have not ticked the option");
        }
    }
}