/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit.gui;

import javax.swing.*;

/**
 *
 * @author kukut
 */

public class Tooltip extends JFrame {

    public Tooltip () {
        super ("Example Swing program");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel label = new JLabel ("Label");
        label.setToolTipText("Tool tip for label");       // <1>
        add (label);

        setSize (400, 200);
    }

    public static void mainn(String[] args) {
        JFrameDemo app = new JFrameDemo ();
        app.setVisible (true);
    }
}