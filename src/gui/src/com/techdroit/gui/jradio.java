/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author kukut
 */

public class jradio {
    
    static JFrame frame = new JFrame("Example");
    static String pieString = "pie";
    static String iceString = "ice";
    static String cakeString = "cake";

    public static void mainn(String[] args) {

        // JRadioButton code
        final JLabel piclabel
                = new JLabel(new ImageIcon( pieString + ".jpg"));

        /** Listens to the radio buttons. */
        class RadioListener implements ActionListener {                 // <1>
                public void actionPerformed(ActionEvent e) {
                // getting the event causes update on Jlabel icon
                piclabel.setIcon(
                        new ImageIcon( e.getActionCommand()+".jpg"));   // <2>
                }
        }

        JRadioButton pieButton = new JRadioButton(pieString);
        pieButton.setMnemonic('b');
        pieButton.setActionCommand(pieString);                    // <3>
        pieButton.setSelected(true);

        JRadioButton cakeButton = new JRadioButton(cakeString);
        JRadioButton iceButton = new JRadioButton(iceString);

        // Group the radio buttons.
        ButtonGroup group = new ButtonGroup();                   // <4>
        group.add(pieButton);
        group.add(cakeButton);
        group.add(iceButton);

        // Register a listener for the radio buttons.
        RadioListener myListener = new RadioListener();          // <5>
        pieButton.addActionListener(myListener);
        cakeButton.addActionListener(myListener);
        iceButton.addActionListener(myListener);

        // Put the radio buttons in a column in a panel to line up
        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new GridLayout(0, 1));             // <6>
        radioPanel.add(pieButton);
        radioPanel.add(cakeButton);
        radioPanel.add(iceButton);

        frame.getContentPane().add(radioPanel);
        frame.getContentPane().add(piclabel);
        frame.setVisible(true);

        frame.setSize(400,100);
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout( new FlowLayout() );
        frame.getContentPane().add( radioPanel );
        frame.pack(); // size the frame to fit its contents
        frame.setVisible(true);
    }
}