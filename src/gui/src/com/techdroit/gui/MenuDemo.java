/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit.gui;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author kukut
 */
public class MenuDemo extends JFrame {

    public MenuDemo() {
        super("Sample frame"); // create JFrame with a title
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // create and add menu bar
        JMenuBar mb = new JMenuBar();                      // <1>
        setJMenuBar(mb);

        // create menu 1
        JMenu fileMenu = new JMenu("File");                // <2>
        mb.add(fileMenu);  // add menu to the menu bar
        // create an 'exit program' menu item
        JMenuItem exitItem = new JMenuItem("Exit");        // <3>
        exitItem.addActionListener(new ExitListener());    // <4>
        fileMenu.add(exitItem);

        // create menu 2
        JMenu helpMenu = new JMenu("Help");
        mb.add(helpMenu);  // add menu to the menu bar
        // create an 'about' menu item
        JMenuItem aboutItem = new JMenuItem("About");
        aboutItem.addActionListener(new AboutListener());
        helpMenu.add(aboutItem);

        setSize(400, 200);     // set size to width 400 pixels, height 200
    }

    public static void mainn(String[] args) {
        MenuDemo app = new MenuDemo();
        app.setVisible(true);  // show the application
    }
}

class AboutListener implements ActionListener {

    public AboutListener() {
        super();
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println("About the program");
    }
}

class ExitListener implements ActionListener {

    public ExitListener() {
        super();
    }

    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
