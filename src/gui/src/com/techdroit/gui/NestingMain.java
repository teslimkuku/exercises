/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit.gui;

/**
 *
 * @author kukut
 */
import java.awt.BorderLayout;
import javax.swing.*;

public class NestingMain extends JFrame {

    public NestingMain () {
        super ("Example Swing program with nested components");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout (new BorderLayout());

        // we will place an editor pane in the middle
        JEditorPane pane = new JEditorPane ();
        JScrollPane jsp = new JScrollPane (pane); // wrap editor in a scrolling container
        add(jsp, BorderLayout.CENTER);

        // a title is placed on top
        JLabel label = new JLabel ("Enter some text in this window");
        add (label, BorderLayout.NORTH);

        // now add a panel with buttons in them
        add (makeButtonPanel (), BorderLayout.EAST);              // <1>

        setSize (400, 200);
    }

    public static void mainn(String[] args) {
        NestingMain app = new NestingMain ();
        app.setVisible (true);
    }

    public JPanel makeButtonPanel () {
      JPanel panel = new JPanel ();                               // <2>
      panel.setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS));  // <3>

      panel.add (new JButton ("Save"));                           // <4>
      panel.add (new JButton ("Load"));
      panel.add (new JButton ("Clear"));

      return panel;
    }
}