/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit.gui;

/**
 *
 * @author kukut
 */
import javax.swing.*;

public class BoxLayoutDemo extends JFrame {

    public BoxLayoutDemo () {
        super ("Example Swing program");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout (new BoxLayout(getContentPane (), BoxLayout.Y_AXIS));   // <1>

        JLabel label1 = new JLabel ("Label 1");
        label1.setToolTipText("Tool tip for label 1");
        add (label1);                                                     // <2>

        JLabel label2 = new JLabel ("Label 2");
        label2.setToolTipText("Tool tip for label 2");
        add (label2);

        setSize (400, 200);
    }

    public static void mainn(String[] args) {
        BoxLayoutDemo app = new BoxLayoutDemo ();
        app.setVisible (true);
    }
}