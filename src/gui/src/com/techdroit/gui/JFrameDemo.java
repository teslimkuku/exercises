/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit.gui;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author kukut
 */
public class JFrameDemo extends JFrame {                                        // <1>

    public JFrameDemo() {
        super("Sample frame"); // create JFrame with a title              // <2>

        // tell the frame how to handle clicks on the 'close window' button
        // usually in the upper right of the frame
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);                   // <3>

        // create a button - this is a widget/control
        JButton button = new JButton("Click me!");                        // <4>
        // add an ActionListener to respond to a button click
        button.addActionListener(new ClickListener());                    // <5>
        // add the button to the frame
        add(button);                                                      // <6>

        setSize(400, 200);     // set frame size to width 400 pixels, height 200
    }

    /**
     * @param args the command line arguments
     */
    public static void mainn(String[] args) {
        JFrameDemo app = new JFrameDemo();
        app.setVisible(true);  // show the application                    // <7>
    }
}

// this class implements an ActionListener which is attached
// to the button 'clicked' event
class ClickListener implements ActionListener {                           // <8>
    public ClickListener () {
        super ();
    }

    // this method provides the functionality when the button is clicked
    public void actionPerformed(ActionEvent e) {                          // <9>
        System.out.println("Clicked");
    }
}