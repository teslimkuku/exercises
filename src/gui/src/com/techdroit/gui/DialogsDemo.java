/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit.gui;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author kukut
 */

public class DialogsDemo extends JFrame {

    public DialogsDemo() {
        super("Sample frame"); // create JFrame with a title

        // tell the frame how to handle clicks on the 'close window' button
        // usually in the upper right of the frame
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // create a button
        JButton button = new JButton("Click me!");
        // add an ActionListener to respond to a button click
        button.addActionListener(new ClickListener1());
        // add the button to the frame
        add(button);

        setSize(400, 200);     // set size to width 400 pixels, height 200
    }

    public static void main(String[] args) {
        DialogsDemo app = new DialogsDemo();
        app.setVisible(true);  // show the application
    }
}

// this class implements an ActionListener which is attached
// to the button 'clicked' event
class ClickListener1 implements ActionListener {
    public ClickListener1 () {
        super ();
    }

    // this method provides the functionality when the button is clicked
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog (null,
                "There was an error in your program",
                "Program error",
                JOptionPane.ERROR_MESSAGE);
    }
}