/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit1.task5;

/**
 *
 * @author Teslim
 */
public class WaterTransport extends Transport{
 
    private int numberOfSails;
    
    public WaterTransport(String owner, int numberOfPassengers, int numberOfWheels, String color, 
            int numberOfSails){
        super(owner, numberOfPassengers, numberOfWheels, color);
        this.numberOfSails = numberOfSails;
    }
}