/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit1.task4;

/**
 *
 * @author Teslim
 */
public class RealCar {

    private String engine;
    private String make;                         // <1>
    private String registrationNumber;
    public String colour;
    private static int numberOfRegisteredCars;
    private static int fordCount;
    private static int bmwCount;
    private static int toyotaCount;
    private static int mazdaCount;

    public RealCar(String engine, String make, String registrationNumber, String colour) {
        this.engine = engine;
        this.make = make;
        this.registrationNumber = registrationNumber;
        this.colour = colour;

        numberOfRegisteredCars++;
        
        if (make.equals("Ford")) {
            fordCount++;
        }

        if (make.equals("BMW")) {
            bmwCount++;
        }

        if (make.equals("Toyota")) {
            toyotaCount++;
        }

        if (make.equals("Mazda")) {
            mazdaCount++;
        }
    }

    public static void printMakeCount() {

        System.out.println("Ford count: " + fordCount);
        System.out.println("BMW count: " + bmwCount);
        System.out.println("Toyota count: " + toyotaCount);
        System.out.println("Mazda count: " + mazdaCount);

    }

    // Constructor;
    // registration and make must be specified
    public RealCar(String registrationNumber, String make) {
        this.registrationNumber = registrationNumber;
        this.make = make;

        /*
        if (registrationNumber == null )
        {
          //Registration Number can't be null - see discussion on EXCEPTIONS below

        }
         */
 /*
        if ( make == null ){
          //make can't be null - see discussion on EXCEPTIONS below

        }
         */
    }

    // return value of instance variable, make.
    public String getMake() {                    // <2>
        return make;
    }

    // return value of instance variable, Registration Number.
    public String getReg() {
        return registrationNumber;
    }

    public static void getNumberOfRegisteredCars() {
        System.out.println("Total Number of cars: " + numberOfRegisteredCars);
    }
    
    @Override
    public String toString(){
        return "{engine:"+engine+",make:"+make+",regNumber:"+registrationNumber+",color:"+colour+"}";
    }
}
