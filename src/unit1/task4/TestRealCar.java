/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit1.task4;

import java.util.Random;

/**
 *
 * @author Teslim
 */
public class TestRealCar {
 
    public static void main(String[] args) {
        
        //RealCar car1 = new RealCar("Engine1","Ford","BDG154DX","Blue");
        //RealCar car2 = new RealCar("Engine2","Ford","AAK154DX","Green");
        //RealCar car3 = new RealCar("Engine3","Ford","BDG154DX","Red");
   
        
        String[] engines = {"V1","V2","V3","V4"};
        String[] makes = {"Ford","BMW","Toyota","Mazda"};
        String[] colors = {"Black","Ash","Blue","Red"};
        
        RealCar[] realCars = new RealCar[10];
        Random rnd = new Random();
        
        for(int i = 0; i < realCars.length; i++){
            
            int x = rnd.nextInt(4);
            String regNumber = "BDG15" + (i + 1) + "DX";
            realCars[i] = new RealCar(engines[x],makes[x],regNumber,colors[x]);
            System.out.println(realCars[i]);
        }
        
        RealCar.getNumberOfRegisteredCars();
        RealCar.printMakeCount();
    }
}