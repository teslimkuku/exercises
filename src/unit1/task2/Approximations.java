/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit1.task2;

/**
 *
 * @author Teslim
 */
public class Approximations {

    public static void main(String[] args) {

        //Approximations.floatApproximation(10);
        //Approximations.doubleApproximation(10);
        for (int i = 0; i < 100; i++) {
            System.out.println(i + "\t\t"
                    + Approximations.floatApproximation(i) + "\t\t"
                    + Approximations.doubleApproximation(i));
        }
    }

    public static float floatApproximation(int x) {

        float diff = 1.0F / 10;
        float sum = 0.0F;

        for (int i = 0; i < x; ++i) {
            sum += diff;
        }

        /*if (sum == 1.0F) {
            System.out.println("Equals 1.0");
        } else {
            System.out.println("Does not equal 1.0");
        }*/
        return sum;
    }

    public static double doubleApproximation(int x) {

        double diff = 1.0 / 10;
        double sum = 0.0;

        for (int i = 0; i < x; ++i) {
            sum += diff;
        }

        //System.out.println(sum);
        /*if (sum == 1.0) {
            System.out.println("Equals 1.0");
        } else {
            System.out.println("Does not equal 1.0");
        }*/
        return sum;
    }
}
