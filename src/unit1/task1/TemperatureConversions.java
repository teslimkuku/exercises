/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit1.task1;

/**
 *
 * @author Teslim
 */
public class TemperatureConversions {
    
    public double convertFarenheitToCelcius(double fahrenheit){
        
        double celcius = (fahrenheit-32)*5/9;
        return celcius;
    }
    
    public void temperatureConversionTable(double start, double end, double stepSize){
        
        for(double i = start; i <= end; i+= stepSize){
            
            System.out.printf("%.0f\t%.0f\n",i,convertFarenheitToCelcius(i));
        }
    }
    
    public static void main(String[] args) {
        
        TemperatureConversions tc = new TemperatureConversions();
        
        //double fahrenheitTemp = 56;
        //double celciusTemp = tc.convertFarenheitToCelcius(fahrenheitTemp);
        //System.out.printf("%.2f fahrenheit in celcius is: %.2f\n",fahrenheitTemp,celciusTemp);
        
        //tc.temperatureConversionTable(5, 10, 2);
        tc.temperatureConversionTable(50, 100, 20);
    }
}
/**
    Write a program to generate a table of temperature conversions, converting from fahrenheit to celsius:
    
    Write a function to convert a single temperature from fahrenheit to celsius. 
    If F is the temperature in degrees fahrenheit, the temperature in degrees celsius is (F-32)*5/9.
    
    Write a function which takes a start, end and step size, and prints a table of temperature conversions. 
    For example, if you call it with start 5, end 10 and step 2, it will print a line for 5, 7 and 9.
    
    Call your functions with different values, print the results, and make sure they are working correctly.
 */