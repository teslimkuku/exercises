/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellowworld;

/**
 *
 * @author Teslim
 */
public class OldRealCar {
    String engine;
    String color;
    String make;
    int numberOfRegisteredCars;

    OldRealCar(String petrolOrDiesel, String color, String make) {
        engine = petrolOrDiesel;
        this.color = color;
        this.make = make;
    }
    
    public void setNumberOfRegisteredCars(int numberOfRegisteredCars){
        this.numberOfRegisteredCars = numberOfRegisteredCars;
    }
    
    public int getNumberOfRegisteredCars(){
        return numberOfRegisteredCars;
    }

    String write() {
        return "This car has this type of engine: " + engine
                +"\nThis car has this type of color: " + color
                +"\nThis car has this type of make: " + make;
    }
}