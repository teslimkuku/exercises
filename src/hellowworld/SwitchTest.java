/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellowworld;

/**
 *
 * @author Teslim
 */
public class SwitchTest {

    public static void main(String[] args) {

        int myvar = 6;
        switch (myvar) {                    // <1>
            case 1:                           // <2>
                System.out.println("one");      // <3>
                break;                          // <4>

            case 2:
                System.out.println("two");
                break;

            case 3:
                System.out.println("three");
                break;

            case 4:
            case 5:
            case 6:           // <5>
                System.out.println("several");
                break;

            default:                          // <6>
                System.out.println("many");
                break;
        }
    }
}
