/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellowworld;

/**
 *
 * @author Teslim
 */
public class Main {

    static double convertCelsiusToFahrenheit (double celsius) {
        return (celsius * 9 / 5) + 32;
    }
    
    static double convertFahrenheitToCelsius(double fahrenheit){
        return (fahrenheit * 5 / 9) - 32;
    }

    public static void main(String[] args) {
        //double smallest = 0;
        //double largest = 100;
        
        double smallest = 32;
        double largest = 212;

        for (double i = smallest; i <= largest; ++i) {
            //System.out.print(i + " in fahrenheit = ");
            System.out.print(i + " in celcius = ");
            //System.out.format("%.2f", convertCelsiusToFahrenheit (i));
            System.out.format("%.2f", convertFahrenheitToCelsius(i));
            System.out.println();
        }
    }
}