/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellowworld;

/**
 *
 * @author Teslim
 */
public class RealCar {
    
   private String engine;
   private String make;                         // <1>
   private String registrationNumber;
   public String colour;
   public static int numberOfRegisteredCars;

   public RealCar(String engine, String registrationNumber, String make){
       this.engine = engine;
       this.registrationNumber = registrationNumber;
       this.make = make;
   }
   
   // Constructor;
   // registration and make must be specified
   public RealCar(String registrationNumber, String make) {
        this.registrationNumber = registrationNumber;
        this.make = make;

        /*
        if (registrationNumber == null )
        {
          //Registration Number can't be null - see discussion on EXCEPTIONS below

        }
        */
        /*
        if ( make == null ){
          //make can't be null - see discussion on EXCEPTIONS below

        }
        */
   }

   // return value of instance variable, make.
   public String getMake() {                    // <2>
      return make;
   }

  // return value of instance variable, Registration Number.
  public String getReg() {
      return registrationNumber;
   }
}