/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellowworld;

/**
 *
 * @author Teslim
 */
public class TestMyCar {

    public static void main(String[] args) {

        //OldRealCar car1 = new OldRealCar("Petrol","Red","Honda");
        //OldRealCar car2 = new OldRealCar("Diesel","Green","Camry");
        //ModelCar myToy = new ModelCar("Clockwork");
        //System.out.println(myCar.write());
        //System.out.println(myToy.write());
        //RealCar myCar = new RealCar( "ABC 123D", "Toyota");
        //System.out.println(myCar.getMake());
        FamilyCar myCar = new FamilyCar("CD Player", "Petrol", "ABC 123D", "Toyota");

        System.out.println(myCar.getMake());                // <1>
        System.out.println(myCar.getReg());
        System.out.println(myCar.getEntertainmentSystem());
        //System.out.println(myCar.getNumberOfCars());

        RealCar myCar2 = new RealCar("Diesel", "DEF 123G", "BMW");

        System.out.println(myCar2.getMake());
        System.out.println(myCar2.getReg());
        //System.out.println(myCar2.getNumberOfCars());
    }
}
