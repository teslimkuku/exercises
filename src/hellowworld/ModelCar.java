/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellowworld;

/**
 *
 * @author Teslim
 */
public class ModelCar {
    String engine;

    ModelCar(String engine) {
        this.engine = engine;  // <1>
    }

    String write() {
        return "My model car is powered by  " + engine;
    }
}