/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellowworld;

/**
 *
 * @author Teslim
 */
public class Converter {

    public static int readNumber (String str) {
        int result = 0;

        try {                                           // <1>
            result = Integer.parseInt (str);            // <2>
        } catch (NumberFormatException ex) {            // <3>
            // the string is not a valid integer, so leave result as 0,
            // but display a message.
            System.out.println ("Read Number failed to parse " + str);
        }

        return result;
    }

    public static void main (String[] args) {
      int sum = 0;
      sum = sum + readNumber ("12");
      sum = sum + readNumber ("x");

      System.out.println ("Sum is " + sum);
    }
}