/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellowworld;

/**
 *
 * @author Teslim
 */
public class FamilyCar  extends RealCar {           // <1>
  private String entertainmentSystem;

  public FamilyCar(String entertainmentSystem,
                   String engine,
                   String registrationNumber,
                   String make) {
    super(engine, registrationNumber, make);        // <2>
    this.entertainmentSystem = entertainmentSystem;
  }

  // accessor to entertainmentSystem field
  public String getEntertainmentSystem() {
      return entertainmentSystem;
  }
}