/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellowworld;

/**
 *
 * @author Teslim
 */
public class CastTest
{
    public static void main(String[] args)
    {
      double d = 99.07;
      long l = (long)d;  //explicit type casting required
      int i = (int)l;   //explicit type casting required

      System.out.println("Double value "+d);
      System.out.println("Long value "+l);
      System.out.println("Int value "+i);

    }
}