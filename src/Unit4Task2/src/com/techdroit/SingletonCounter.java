/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit;

/**
 *
 * @author kukut
 */
public class SingletonCounter {

    public static SingletonCounter instance = new SingletonCounter();
    private int count = 0;

    private SingletonCounter() {
    }

    public int NextValue() {
        return ++count;
    }
}
