/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techdroit;

/**
 *
 * @author kukut
 */
public class SingletonClient {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println("Next singleton value: " + SingletonCounter.instance.NextValue());
        }
    }
}