/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sayhello;

/**
 *
 * @author Teslim
 */
public class Converter {

    static double convertCelsiusToFahrenheit(double celsius) {
        return (celsius * 9 / 5) + 32;
    }

    public static void main(String[] args) {
        double smallest = Double.parseDouble(args[0]);
        double largest = Double.parseDouble(args[1]);

        for (double i = smallest; i <= largest; ++i) {
            System.out.print(i + " in fahrenheit = ");
            System.out.format("%.2f", convertCelsiusToFahrenheit(i));
            System.out.println();
        }
    }
}