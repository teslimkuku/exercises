/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sayhello;

/**
 *
 * @author Teslim
 */
public class SayHello {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        if (args.length == 0) {
            System.out.println("Hello.  Please tell me your name next time.");
        } else {
            System.out.println("Hello " + args[0]);
        }

        /*if (args.length == 0) {
            System.out.println("You must type in something");
        } else {
            try {
                int value = Integer.parseInt(args[0]);
                System.out.println("Double of " + value + " is " + 2 * value);
            } catch (NumberFormatException nfe) {
                System.out.println("You must provide a number");
            }
        }*/
    }
}
