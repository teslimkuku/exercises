/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sayhello;

/**
 *
 * @author Teslim
 */
public class DRY {

    public static void main(String[] args) {
        double number1 = Double.parseDouble(args[0]);
        double number2 = Double.parseDouble(args[1]);
        doConversion(number1);
        doConversion(number2);
    }

    private static void doConversion(double number1) {
        // Let us compute the fahrenheit equivalent of the first number
        double fahr1 = (number1 * 9 / 5) + 32;
        System.out.println("Converting " + number1 + " to fahrenheit gives " + fahr1);
        // Let us compute the fahrenheit equivalent of the second number
    }
}
